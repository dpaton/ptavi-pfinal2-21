#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socketserver
import socket
import sys
import json
import time

SERVER = 'localhost'
try:
    PORT = int(sys.argv[1])
    fichero = sys.argv[2]
except IndexError:
        print("Usage: python3 serverproxy.py <port> <file>")

tiemp = time.strftime("%Y%m%d%H%M%S", time.gmtime())


class SIPRegisterHandler(socketserver.DatagramRequestHandler):

    dic = {}



    def handle(self):

        response = "SIP/2.0 200 OK" "\r\n" + "\r\n"
        fallo = "SIP/2.0 400 Bad Request" "\r\n" + "\r\n"
        linea = self.rfile.read()
        metodo = linea.decode('utf-8').split(" ")[0]
        print("estoy dentro")

        if metodo == "INVITE":
            try:
                with open("register.json", "r") as jsonfile:
                    self.dic = json.load(jsonfile)
                    print(self.dic)

                    usuario = linea.decode('utf-8').split(":")[1].split(" ")[0]
                    mensaje = linea.decode('utf-8').split("\r\n")[0]
                    print(usuario)
                    if usuario in self.dic:
                        a = self.dic[usuario][0].split(" ")[1].split(":")[0]
                        b = int(self.dic[usuario][0].
                                split(" ")[1].split(":")[1])
                        print(f"{tiemp} SIP from {a}:{b} {mensaje}.")
                        with socket.socket(socket.AF_INET,
                                           socket.SOCK_DGRAM) as my_socket:

                            peticion = linea
                            my_socket.sendto(peticion, (a, b))
                            data = my_socket.recv(2048)
                            sip = data.decode('utf-8')
                            respuesta = sip.split("\r\n")[0]

                            if respuesta == "SIP/2.0 200 OK":
                                print(f"{tiemp} SIP to {a}:{b} {respuesta}.")
                                self.wfile.write(data)

            except IndexError:
                pass
        elif metodo == "ACK":
            try:
                with open("register.json", "r") as jsonfile:
                    self.dic = json.load(jsonfile)
                    usuario = linea.decode('utf-8').split(":")[1].split(" ")[0]
                    if usuario in self.dic:
                        a = self.dic[usuario][0].split(" ")[1].split(":")[0]
                        b = int(self.dic[usuario][0].
                                split(" ")[1].split(":")[1])
                        mensaje = linea.decode('utf-8').split("\r\n")[0]
                        print(f"{tiemp} SIP from {a}:{b} {mensaje}.")
                        with socket.socket(socket.AF_INET,
                                           socket.SOCK_DGRAM) as my_socket:

                            peticion = linea
                            my_socket.sendto(peticion, (a, b))
                            data = my_socket.recv(2048)
                            sip = data.decode('utf-8')
                            respuesta = sip.split("\r\n")[0]

            except IndexError:
                pass
        elif metodo == "BYE":
            try:
                with open("register.json", "r") as jsonfile:
                    self.dic = json.load(jsonfile)
                    usuario = linea.decode('utf-8').split(":")[1].split(" ")[0]
                    if usuario in self.dic:
                        a = self.dic[usuario][0].split(" ")[1].split(":")[0]
                        b = int(self.dic[usuario][0].
                                split(" ")[1].split(":")[1])
                        mensaje = linea.decode('utf-8').split("\r\n")[0]
                        print(f"{tiemp} SIP from {a}:{b} {mensaje}.")
                        with socket.socket(socket.AF_INET,
                                           socket.SOCK_DGRAM) as my_socket:

                            peticion = linea
                            my_socket.sendto(peticion, (a, b))
                            data = my_socket.recv(2048)
                            sip = data.decode('utf-8')
                            respuesta = sip.split("\r\n")[0]

                            if respuesta == "SIP/2.0 200 OK":
                                print(f"{tiemp} SIP to {a}:{b} {respuesta}.")
                                self.wfile.write(data)
            except IndexError:
                """En caso de que no este creado continuamos para crearlo."""
                pass

        else:
            self.wfile.write(fallo.encode('utf-8'))


def main():

    try:
        serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
        print(f"{tiemp} Starting...")

    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
        socketserver.TCPServer.allow_reuse_address = True
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
