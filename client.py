#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys
import socketserver
import time
import threading
from recv_rtp import RTPHandler

try:
    SERVER = 'localhost'
    IPReg = str(sys.argv[1].split(':')[0])
    puertoReg = int(sys.argv[1].split(':')[1])
    IPProxy = str(sys.argv[2].split(':')[0])
    puertoProxy = int(sys.argv[2].split(':')[1])
    dirCliente = sys.argv[3]
    dirServRTP = sys.argv[4]
    tiempo = sys.argv[5]
    fichero = sys.argv[6]

except IndexError:
        print("Usage: python3 client.py <IPReg>:<portReg>"
              " <IPProxy>:<portProxy> <addrClient>"
              " <addrServerRTP> <time> <file>")
tiemp = time.strftime("%Y%m%d%H%M%S", time.gmtime())


def main():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.connect((SERVER, puertoReg))
        ip = my_socket.getsockname()[0]
        puerto = my_socket.getsockname()[1]
        direccion = ip + " " + str(puerto)
        peticion = "REGISTER " + dirCliente +\
                   " SIP/2.0" + "\r\n" + direccion + "\r\n"
        mensaje = peticion.split("\r\n")[0]
        print(f"{tiemp} SIP to {ip}:{puerto} {mensaje}.")
        my_socket.sendto(bytes(peticion, 'utf-8'), (SERVER, puertoReg))
        data = my_socket.recv(2048)
        sip = data.decode('utf-8')
        respuesta = sip.split("\r\n")[0]

        if respuesta == "SIP/2.0 200 OK":
            my_socket.close()
            print("cerrando conexion con Registrar")
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                my_socket.connect((SERVER, puertoProxy))
                ip = my_socket.getsockname()[0]
                puerto = my_socket.getsockname()[1]

                peticion = "INVITE " + dirServRTP + " SIP/2.0" + "\r\n" + \
                           "Content-Type: application/sdp" + \
                           "\r\n\r\nv=0\r\no=yo@clientes.net 127.0.0.1" \
                           "\r\ns=heyjude\r\nt=0\r\nm=audio 34543 RTP\r\n"
                mensaje = peticion.split("\r\n")[0]
                print(f"{tiemp} SIP to {ip}:{puerto} {mensaje}.")
                my_socket.sendto(bytes(peticion, 'utf-8'), (SERVER, puertoProxy))
                data2 = my_socket.recv(2048)
                sip2 = data2.decode('utf-8')
                respuesta2 = sip2.split("\r\n")[0]
                print(f"{tiemp} SIP from {ip}:{puerto} {respuesta2}.")

                if respuesta2 == "SIP/2.0 200 OK":

                    RTPHandler.open_output(fichero)
                    try:

                        with socketserver.UDPServer((SERVER,
                                                     0), RTPHandler) as serv:
                            puerto = serv.server_address[1]
                            ipp = serv.server_address[0]
                            dir = ipp + " " + str(puerto)
                            print(dir)
                            peticion = "ACK " + dirServRTP +\
                                       " SIP/2.0" + "\r\n" + dir + "\r\n"
                            mensaje = peticion.split("\r\n")[0]
                            print(f"{tiemp} SIP to {ip}:{puerto} {mensaje}.")
                            my_socket.sendto(bytes(peticion, 'utf-8'),
                                             (SERVER, puertoProxy))
                            threading.Thread(target=serv.serve_forever).start()
                            time.sleep(10)
                            peticion2 = "BYE " + dirServRTP + " SIP/2.0" + "\r\n"
                            mensaje = peticion2.split("\r\n")[0]
                            print(f"{tiemp} SIP to {ip}:{puerto} {mensaje}.")
                            my_socket.sendto(bytes(peticion2, 'utf-8'),
                                             (SERVER, puertoProxy))
                            print("Time passed, shutting down receiver loop.")
                            data3 = my_socket.recv(2048)
                            if data3 == b"":
                                data3 = my_socket.recv(2048)
                                sip3 = data3.decode('utf-8')
                                respuesta3 = sip3.split("\r\n")[0]
                                print(f"{tiemp} SIP from {ip}:{puerto} {respuesta2}.")

                                if respuesta3 == "SIP/2.0 200 OK":
                                    serv.shutdown()
                            RTPHandler.close_output()
                    except ConnectionRefusedError:
                        print("Error de conexion")





if __name__ == "__main__":
    main()