#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socketserver
import sys
import json
import time

SERVER = 'localhost'
try:
    PORT = int(sys.argv[1])
    fichero = sys.argv[2]
except IndexError:
        print("Usage: python3 serverreg.py <port> <file>")

tiemp = time.strftime("%Y%m%d%H%M%S", time.gmtime())


class SIPRegisterHandler(socketserver.DatagramRequestHandler):

    dic = {}

    def register2json(self):
        fich = "register.json"
        with open(fich, "w") as fiche:
            json.dump(self.dic, fiche, indent=3)

    def json2registered(self):
        """Comprobar que el archivo no este creado."""
        try:
            with open("registered.json", "r") as jsonfile:
                self.mensaje = json.load(jsonfile)
        except IndexError:
            """En caso de que no este creado continuamos para crearlo."""
            pass

    def handle(self):

        response = "SIP/2.0 200 OK" "\r\n" + "\r\n"
        fallo = "SIP/2.0 400 Bad Request" "\r\n" + "\r\n"
        linea = self.rfile.read()
        metodo = linea.decode('utf-8').split(" ")[0]

        if metodo == "REGISTER":
            usuario = linea.decode('utf-8').split(":")[1].split(" ")[0]
            IP = str(linea.decode("utf-8").split("\r\n")[1].split(" ")[0])
            port = int(linea.decode("utf-8").split("\r\n")[1].split(" ")[1])
            mensaje = linea.decode('utf-8').split("\r\n")[0]
            print(f"{tiemp} SIP from {IP}:{port} {mensaje}.")
            direcc = str(IP) + ":" + str(port)
            self.dic[usuario] = ["address: " + direcc]
            self.register2json()
            self.wfile.write(response.encode('utf-8'))  # respuesta


        else:
            self.wfile.write(fallo.encode('utf-8'))


def main():

    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)

        print(f"{tiemp} Starting...")

    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
        socketserver.TCPServer.allow_reuse_address = True
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
