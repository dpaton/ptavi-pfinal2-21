#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
import time
import socket
import socketserver
import sys
import simplertp

try:
    SERVER = 'localhost'
    IPReg = str(sys.argv[1].split(':')[0])
    puertoReg = int(sys.argv[1].split(':')[1])
    fichero = sys.argv[2]
    tiemp = time.strftime("%Y%m%d%H%M%S", time.gmtime())
except IndexError:
        print("Usage: python3 serverrtp.py <IPReg>:<portReg> <file>")

def enviar(dir):

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            direccion = fichero.split(".")[0] + "@signasong.net"
            peticion = "REGISTER" + " sip:" + direccion +\
                       " SIP/2.0" + "\r\n" + dir + "\r\n"
            my_socket.sendto(bytes(peticion, 'utf-8'), (SERVER, puertoReg))
            datap = my_socket.recv(2048)
            sip = datap.decode('utf-8')
            mensaje = peticion.split("\r\n")[0]
            print(f"{tiemp} SIP to {IPReg}:{puertoReg} {mensaje}.")
    except ConnectionRefusedError:
        print("Error conectando servidor")

class EchoHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """
    dic = {}

    def handle(self):

        global sender

        data = self.rfile.read().decode("utf-8")
        receptor = data.split(":")[1].split(" ")[0]

        metodo = data.split(" ")[0]
        self.dic[receptor] = [" " + receptor.split("@")[0]]
        if metodo in ["INVITE", "BYE", "ACK"]:
            if data.split(" ")[1].split(":")[0] != "sip":
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n")
            else:

                if metodo == 'INVITE':
                    resp = b"SIP/2.0 200 OK\r\n"
                    self.wfile.write(resp)
                    mensaje = data.split("\r\n")[0]
                    print(f"{tiemp} SIP from {IPReg}:{puertoReg} {mensaje}.")

                if metodo == 'ACK':
                    IP = str(data.split("\r\n")[1].split(" ")[0])
                    port = int(data.split("\r\n")[1].split(" ")[1])
                    mensaje = data.split("\r\n")[0]
                    print(f"{tiemp} SIP from {IP}:{port} {mensaje}.")
                    sender = simplertp.\
                        RTPSender(ip=IP, port=port,
                                  file='cancion.mp3', printout=True)

                    sender.send_threaded()

                    time.sleep(10)

                    print("Finalizando el thread de envío.")

                if metodo == 'BYE':
                    resp = b"SIP/2.0 200 OK\r\n"
                    sender.finish()
                    self.wfile.write(resp)
                    print(f"{tiemp} SIP from {IPReg}:{puertoReg} {metodo}.")
                    print(f"{tiemp} SIP to {IPReg}:{puertoReg} {resp}.")

        else:
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n")





def main():

    try:
        serv = socketserver.UDPServer((SERVER, 0), EchoHandler)
        print("Listening...")
        puerto = serv.server_address[1]
        ipp = serv.server_address[0]
        dir = ipp + " " + str(puerto)
        enviar(dir)
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Fin de conexion")
        sys.exit(0)


if __name__ == "__main__":
    main()